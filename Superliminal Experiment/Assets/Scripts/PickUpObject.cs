using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    private GameObject heldObject;
    private Rigidbody heldObjectRB;

    private Vector3 originalHeldObjScale;
    private float originalHeldObjDistance;
    private float originalWeight;
    private float originalExplosionPower;
    private float originalExplosionDistance;

    private Vector3 lastHitPoint;
     

    [SerializeField] private float maxScaleMagnitudeClampGrabbed = 30f;
    [SerializeField] private float minScaleMagnitudeClampGrabbed = 0.5f;
    [SerializeField] private float maxScaleMagnitudeClampBeforeGrab = 15f;
    [SerializeField] private float grabDistanceClampMin = 3f;
    [SerializeField] private float grabDistanceClampMax = 30f;
    [SerializeField] private float weightClampMin = 0.5f;
    [SerializeField] private float weighyClampMax = 200f;


    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        lastHitPoint = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(heldObject == null)
            {
                RaycastHit hit;
                if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit)){
                    PickupObject(hit.transform.gameObject);
                }
            }
            else
            {
                heldObjectRB.isKinematic = false;
                heldObjectRB.detectCollisions = true;
                heldObject.layer = 0;
                foreach (Transform child in heldObject.transform)
                {
                    child.gameObject.layer = 0;
                }
                heldObject = null;
                heldObjectRB = null;
            }
        }
        if(heldObject != null)
        {
            ResizeTarget();
        }
    }

    void PickupObject(GameObject pickObj)
    {
        if(pickObj.GetComponent<Interactables>() != null)
        {
            heldObject = pickObj;
            heldObjectRB = pickObj.GetComponent<Rigidbody>();
            heldObjectRB.isKinematic = true;
            heldObjectRB.detectCollisions = false;
            originalHeldObjScale = pickObj.transform.localScale;
            originalHeldObjScale = Vector3.ClampMagnitude(originalHeldObjScale, maxScaleMagnitudeClampBeforeGrab);
            originalHeldObjDistance = Vector3.Distance(Camera.main.transform.position, pickObj.transform.position);
            originalHeldObjDistance = Mathf.Clamp(originalHeldObjDistance, grabDistanceClampMin, grabDistanceClampMax);
            originalWeight = heldObjectRB.mass;
            originalWeight = Mathf.Clamp(originalWeight, weightClampMin, weighyClampMax);
            originalExplosionPower = heldObject.GetComponent<Interactables>().power;
            originalExplosionDistance = heldObject.GetComponent<Interactables>().radius;
            heldObject.layer = 2;
            foreach(Transform child in heldObject.transform)
            {
                child.gameObject.layer = 2;
            }
        }
    }

    void ResizeTarget()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
        {
            if (hit.point != lastHitPoint)
            {
                heldObject.transform.position = hit.point;
                lastHitPoint = hit.point;
                heldObject.transform.LookAt(Camera.main.transform.position);

                float scale = Vector3.Distance(Camera.main.transform.position, heldObject.transform.position) / originalHeldObjDistance;
                Vector3 newLocalScale = Vector3.ClampMagnitude(originalHeldObjScale * scale, maxScaleMagnitudeClampGrabbed);
                if (newLocalScale.magnitude < minScaleMagnitudeClampGrabbed)
                {
                    newLocalScale = newLocalScale * 1.1f;
                }
                heldObject.transform.localScale = newLocalScale;
                heldObjectRB.mass = originalWeight * scale;
                heldObject.GetComponent<Interactables>().power = originalExplosionPower * scale;
                heldObject.GetComponent<Interactables>().radius = originalHeldObjDistance * scale;

                //check if i'm inside something
                float maxScale = Mathf.Max(Mathf.Max(heldObject.transform.localScale.x, heldObject.transform.localScale.y), heldObject.transform.localScale.z);
                Collider[] collidingWith = Physics.OverlapBox(heldObject.transform.position, new Vector3(maxScale, maxScale,maxScale));

                if (collidingWith.Length > 0 && !(collidingWith.Length == 1 && collidingWith[0].CompareTag("GameController")))
                {
                    Vector3 direction = (Camera.main.transform.position - heldObject.transform.position);
                    if (direction.magnitude > maxScale)
                    {
                        direction.Normalize();
                        heldObject.transform.Translate(Vector3.forward * maxScale);

                        scale = Vector3.Distance(Camera.main.transform.position, heldObject.transform.position) / originalHeldObjDistance;
                        newLocalScale = Vector3.ClampMagnitude(originalHeldObjScale * scale, maxScaleMagnitudeClampGrabbed);
                        heldObject.transform.localScale = newLocalScale;
                        heldObjectRB.mass = originalWeight * scale;
                    }
                }
            }
        }
    }

}
