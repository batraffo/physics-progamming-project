using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableMessageAndStartGame : MonoBehaviour
{
    [SerializeField] private GameObject panelToDisable;
    [SerializeField] private GameObject imageToDisable;
    [SerializeField] private GameObject playerToAble;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            panelToDisable.SetActive(false);
            imageToDisable.SetActive(false);
            playerToAble.GetComponent<PickUpObject>().enabled = true;
            this.GetComponent<DisableMessageAndStartGame>().enabled = false;
        }
    }
}
