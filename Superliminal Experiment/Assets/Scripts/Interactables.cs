using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactables : MonoBehaviour
{

    [Tooltip("The radius of the explosion.")]
    public float radius = 40;
    [Tooltip("The power of the explosion.")]
    public float power = 10000;
    [Tooltip("The time before the explosion (in seconds).")]
    public float explosionCountDown = 3;
    [Tooltip("The frequency with which this object blinks before the explosion.")]
    [Range(1, 10)]
    public int blinkFrequency = 1;
    [Tooltip("The material used for the blink effect.")]
    public Material explosiveMaterial;
    [Tooltip("The particles spawned after the explosion.")]
    public GameObject explosionParticle;

    [SerializeField] private bool gravityInverted = false;
    [HideInInspector] public bool isABomb = false;

    [HideInInspector] public Material defaultMaterial;
    [HideInInspector] public Renderer rend;
    Rigidbody rb;

    void Start()
    {
        rend = GetComponent<Renderer>();
        defaultMaterial = rend.material;
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (gravityInverted)
            rb.AddForce(-Physics.gravity, ForceMode.Acceleration);
    }

    public void InvertGravity()
    {
        gravityInverted = !gravityInverted;
        if (gravityInverted)
        {
            rb.useGravity = false;
        }
        else
        {
            rb.useGravity = true;
        }
    }

    public void BombaTheInteractable()
    {
        if (!isABomb)
        {
            WaitAndExplode();
            isABomb = true;
        }
    }

    void WaitAndExplode()
    {
        StartCoroutine(DetonateCoroutine());
    }

    void ToggleMaterial()
    {
        if (explosiveMaterial != null)
        {
            //Switch from the default material to the explosive material
            rend.material = rend.material == defaultMaterial ? explosiveMaterial : defaultMaterial;
        }
    }

    void Detonate()
    {
        Vector3 explosionPos = transform.position;

        //Cast a sphere around this to find nearby colliders
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);

        //Check if each one found has a Rigidbody
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                //If it does, add explosion force and send it flying
                var realPower = power / Time.timeScale;
                rb.AddExplosionForce(realPower, explosionPos, radius, 1);
            }
        }

        //If the explosion particle is assigned in the Inspector...
        if (explosionParticle != null)
        {
            //Instantiate a particle system
            var particle = Instantiate(explosionParticle, transform.position, explosionParticle.transform.rotation);
            //And then destroy it after 3 seconds
            Destroy(particle, 3);
        }

        //Destroy this gameObject
        Destroy(gameObject);
    }


    IEnumerator DetonateCoroutine()
    {
        float elapsedTime = 0;
        float frameCount = 0;

        while (elapsedTime <= explosionCountDown)
        {
            yield return null;
            elapsedTime += Time.deltaTime;
            frameCount++;

            var blink = Mathf.PingPong(frameCount * Time.timeScale, 10 / blinkFrequency);

            if (blink == 10 / blinkFrequency)
                ToggleMaterial();

            if (elapsedTime >= explosionCountDown)
                Detonate();
        }
    }

}
