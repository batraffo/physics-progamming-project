using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneInteractable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit))
                {
                    if (hit.transform.GetComponent<Interactables>() != null)
                    {
                        Transform newObject = Instantiate(hit.transform);
                        newObject.GetComponent<Interactables>().isABomb = false;
                        newObject.GetComponent<Interactables>().rend.material = newObject.GetComponent<Interactables>().defaultMaterial;
                    }
                }
            }
    }
}
